---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2023'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 10.

---

## 10. Итераторы

1. Основная идея итераторов. Синтаксис
2. Range-based for
3. Категории итераторов
4. std::iterator_traits
5. std::advance
6. const итераторы
7. reverse итераторы
8. std::copy и std::copy_if
9. output итераторы и адаптеры
10. Stream итераторы

---

## 10.1. Основная идея итераторов. Синтаксис

***Def*** Итератор это то, что ведет себя как итератор, то есть:

* Позволяет себя разименовывать (*)
* Позволяет себя передвигать (++)

***Note*** c-style указатель по сути является итератором

---

```c++
std::vector<int> v = {1, 2, 3};
for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
    std::cout << *it << " ";
}
```

***Note*** v.end() это специальный итератор, который указывает на элмент, следущий за последним. Его разыменование это UB!

---

Итератор - это абстракция для доступа к данным. Она нужна нам, потому что далеко не у всех контейнеров можно вызвать [], но у всех можно попросить begin и end:

```c++
std::list<int> v = {1, 2, 3};
for (std::list<int>::iterator it = v.begin(); it != v.end(); ++it) {
    std::cout << *it << " ";
}

v[0]; // CE
```

---

## 10.2. Range-based for

```c++
std::list<int> v = {1, 2, 3};
for (int x : v) {
    std::cout << x << " ";
}
```

На самом деле это превратится в:

```c++
std::list<int> v = {1, 2, 3};
for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
    int x = *it;
    std::cout << x << " ";
}
```

***Note*** вместо явного указания типа лучше использовать auto

---

## 10.3. Категории итераторов

Итераторы подразделяются на категории в зависимости от того какие действия они поддерживают

![w:1080 h:400](/assets/images/iterators_categories.png)

---

## 10.4. std::iterator_traits

Это специальная структура данных, которая позволяет унифицированно работать с итераторами. Сначала посмотрим на использование:

```c++
template <typename Iter>
void foo(Iter it) {
    typename std::iterator_traits<Iter>::value_type val = *it;
}
```

---

Внутри этой структуры есть следующие юзинги:

1. difference_type
2. value_type
3. pointer
4. reference
5. iterator_category

---

iterator_traits определяет эти поля следующим образом:

До c++20: если в Iterator нет хотя бы одного из 5 юзингов то std::iterator_traits пустая. Иначе все юзинги равны соответствующим юзингам из Iterator.

Начиная с c++20: если в Iterator нет юзинга pointer (но есть все остальное), то pointer = void, остальное по той же схеме.

***Note*** iterator_traits можно специализировать для своего класса

---

## 10.5. std::advance

Эта функция позволяет "продвинуть" итератор на n шагов:

```c++
std::list<int> v = {1, 2, 3, 4, 5};
std::list<int>::iterator it = v.begin();
std::advance(it, 3);
std::cout << *it; // 4
```

---

Попробуем реализовать эту функцию:

```c++
template <typename Iterator>
void my_advance(Iterator& iter, int n) {
  iter += n;
}
```

* Проблемы?
* Не работает с не RA итераторами
* Как решать?
* Возьмем iterator_category из traits'ов

---

```c++
#include <iterator>
template <typename Iterator>
void my_advance(Iterator& iter, int n) {
  if (std::is_same_v<typename std::iterator_traits<Iterator>::iterator_category, std::random_access_iterator_tag>) {
    iter += n;
  } else {
    for (int i = 0; i < n; ++i) {
      ++iter;
    }
  }
}

int main() {
  std::list<int> list = {1, 2, 3};
  std::list<int>::iterator it = v.begin();
  my_advance(it, 3);
}
```

* Проблемы?
* CE так как пытаемся компилировать ветку if'а

---

Способ 1: перегрузка функций

```c++
#include <iterator>

template <typename Iterator, typename IteratorCategory>
void helper(Iterator& iter, int n, IteratorCategory) {
  for (int i = 0; i < n; ++i) {
    ++iter;
  }
}

template <typename Iterator>
void helper(Iterator& iter, int n, std::random_access_iterator_tag) {
  iter += n;
}

template <typename Iterator>
void my_advance(Iterator& iter, int n) {
  helper(iter, n, typename std::iterator_traits<Iterator>::iterator_category());
}
```

---

Способ 2: if constexpr (since c++17)

```c++
#include <iterator>
template <typename Iterator>
void my_advance(Iterator& iter, int n) {
  if constexpr (std::is_same_v<typename std::iterator_traits<Iterator>::iterator_category, std::random_access_iterator_tag>) {
    iter += n;
  } else {
    for (int i = 0; i < n; ++i) {
      ++iter;
    }
  }
}
```

---

## 10.6. const итераторы

Это итераторы которые не позволяют менять значения под собой:

```c++
int main() {
  std::list<int> list = {1, 2, 3};
  std::list<int>::const_iterator it = v.cbegin();
  *it = 2; // CE
}
```

---

## 10.7. reverse итераторы

Этот итератор делает все наоборот:

```c++
int main() {
  std::list<int> list = {1, 2, 3};
  for (auto it = v.rbegin(); it != v.rend(); ++it) {
    std::cout << *it;
  }
}
```

---

## 10.8. std::copy и std::copy_if

Эти функции позволяют копировать из src в dst (copy_if копирует с условием (неким предикатом))

```c++
std::list<int> l = {1, 2, 3, 4};
std::vector<int> v;
std::copy(l.begin(), l.end(), v.begin());
```

* Есть ли проблемы в этом коде?
* Да, std::copy не проверяет размер dst

---

## 10.9. output итераторы и адаптеры

Для того чтобы писать в контейнер нам нужен output итератор, а итераторы стандартных контейнеров таковыми не являются.

Воспользуемся адаптором из стандартной библиотеки:

```c++
std::list<int> l = {1, 2, 3, 4};
std::vector<int> v;
std::copy(l.begin(), l.end(), std::back_inserter(v));
```

---

```c++
template <typename Container>
class back_insert_iterator {
 public:
  back_insert_iterator(Container& container): container(container) {}

  back_insert_iterator<Container>& operator++() {
    return *this;
  }

  back_insert_iterator<Container>& operator*() {
    return *this;
  }

  back_insert_iterator<Container>& operator=(const typename container::value_type& value) {
    container.push_back(value);
    return *this;
  }
 private:
  Container& container;
}

template <typename Container>
back_insert_iterator<Container> back_inserter(Container& container) {
  return back_insert_iterator<Container>(container);
}
```

---

Существуют front_insert_iterator(push_front) и просто insert_iterator(insert) (для соответствующих контейнеров)

---

## 10.10. Stream итераторы

Заведем итератор на cin:

```c++
std::istream_iterator<int> it(std::cin);
for (; it != std::istream_iterator<int>(); ++it) {
    std::cout << *it;
    ++it;
}
```
---

Попробуем реализовать:

```c++
template <typename T>
class istream_iterator {
 public:
  istream_iterator(std::istream& in): in_(in) {
    in_ >> value;
  }

  istream_iterator<T>& operator++() {
    in_ >> value;
  }

  T& opeartor*() {
    return value;
  }

 private:
  T value;
  std::istream& in_;
}
```
---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)
