---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2023'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 8.

---

# 7. Динамический полиморфизм и виртуальные функции

5. Разные уточнения
6. Приведение типов в рантайме (dynamic_cast)
7. Устройство виртуальных функций


---

## 7.5. Разные уточнения

### 1. Аргументы по умолчанию

```c++
struct S {
    virtual void f(int a = 10) = 0;
}

struct C: S {
    void f(int a = 20) override {
        std::cout << a;
    }
}

C c;
S& s = c;
s.f();
```

* Что выведется?
* Правильный ответ - 10. То есть аргумент по умолчанию выбирается из того класса из которого мы синтаксически вызываемся

---

### 2. Как вызвать чисто виртуальную функцию?

```c++
struct S {
    virtual void f() = 0;
};
void S::f() {std::cout << 1; }

struct C: S {
    void f() override {std::cout << 2; }
};
```

Чтобы явно вызвать метод родителя можно воспользоваться следующим синтаксисом:

```c++
C c;
c.S::f();
```

---

## 7.6. Приведение типов в рантайме (dynamic_cast)

***Def*** RTTI - динамическая идентификация типа данных

Сейчас мы научимся в runtime понимать какой у нас тип (настоящий). Но зачем?

---

```c++
struct Base {};
struct Derived: Base {};

int main() {
  int x;
  std::cin >> x;
  Base b;
  Derived d;
  Base& b_ref = x % 2 == 0 ? b : d;
}
```

В compile-time понять какой именно тип будет лежать под b_ref невозможно. Поэтому нам нужен какой-то механизм для кастов в run-time. Для этого и нужен dynamic_cast

---

***Def*** Тип называется полиморфным если у него есть хотя бы одна виртуальная функция

dynamic_cast умеет работать только с полиморфными типами (каст от наследника к родителю работает с любыми типами):

```c++
struct Base {};
struct Derived: Base {};

int main() {
  int x;
  std::cin >> x;
  Base b;
  Derived d;
  Base& b_ref = x % 2 == 0 ? b : d;

  dynamic_cast<Derived&>(bb); // CE
}
```

---

dynamic_cast умеет кастить и ссылки и указатели. Но мы пока не знаем что такое исключения поэтому остановимся на указателях:

```c++
struct Base {
  virtual ~Base() = default;
};
struct Derived: Base {};

int main() {
  int x;
  std::cin >> x;
  Base b;
  Derived d;
  Base& b_ref = x % 2 == 0 ? b : d;

  Derived* ptr = dynamic_cast<Derived*>(&bb);
}
```

Если каст будет удачным, то вернется указатель. Если нет, то вернется nullptr.

---

То есть теперь мы умеем писать какие-то такие конструкции:

```c++
void ProcessMsg(AbstractMsg* msg) {
    TxtMsg* txt_msg = dynamic_cast<TxtMsg*>(msg);
    if (txt_msg != nullptr) {
        ...
    }
}
```

---

На самом деле в рантайме мы даже можем узнать имя типа:

```c++
int a;
Derived d;
Base& b = d;
std::cout << typeid(a).name() << " " << typeid(b).name();
```

Оператор typeid:

* Работает для всех типов
* Для полиморфных выводит "настоящий тип"
* Возвращает [std::type_info](https://en.cppreference.com/w/cpp/types/type_info)

---

dynamic_cast умеет кастовать "вбок"

```c++
struct Mother {
};
struct Father {};
struct Son: Mother, Father {
    virtual ~Son() = default;
};

int main() {
  Son s;
  Mother& m = s;
  Dad& d = dynamic_cast<Dad&>(m);
}
```

При этом полимофрным должен быть тип от которого мы кастим (в данном случае Mother)

---

## 7.7. Устройство виртуальных функций

```c++
struct Base {
    virtual void f() {}
    void h();
    int x = 0;
}
```

Размер Base равен 16, так как в нем уже хранится указатель на vtable (таблицу виртуальных функций)
С этой таблицей мы уже сталкивались (виртуальное наследование).

Base сейчас выглядит вот так:  ```[ptr_to_vtable][x][4 bytes of pagging]```

Для Base в этой таблице хранится следующее: ```[ptr to type_info][&Base::f]```

type_info при этом хранится где-то в статической памяти.

---

Добавим наследника

```c++
struct Derived: Base {
    void f() override {}
    virtual void g();
    int y;
}
```

Тогда Derived выглядит так: ```[ptr_to_vtable][x][y]```

VTable для Derived выглядит так: ```[ptr to type_info][&Derived::f][&Derived::g]```

---

Добавим еще звено:

```c++
struct Granny {
    int g;
};
struct Mom: Granny {
    virtual void f() {}
    int m;
};
struct Son: Mom {
    void f() override {}
    int s;
}
```

Тогда Son выглядит так: ```[ptr_to_vtable][g][m][s]```

При этом:

```c++
Son s;
Granny& g = s;
dynamic_cast<Son&>(g); // CE, source type is not polymorphic
```

---

Случай с множественным наследованием мы разбирать не будем. Кому интересно [вкуснятина от Ильи Мещерина](https://www.youtube.com/watch?v=WdA2Lk601CI)

![w:500 h:400](https://princeplaza.ru/wp-content/uploads/2022/09/LogoVkusnoTochka.png)

---

# 8. Всякое разное

1. Перечисления
2. Указатели на члены и методы

---

## 8.1 Перечисления

Существует еще один способ объявить свой тип:

```c++
enum Color {
  RED,
  GREEN,
  BLUE,
  YELLOW
};
```

Это набор именнованных констант. Пользоваться можно так:

```c++
Color c = BLUE;
```

***Так писать не надо***

---

Обычный enum вносит все свои константы в глобальную область видимости. Это не очень хорошо, поэтому в c++11 ввели enum class:

```c++
enum class Color {
    RED,
    GREEN.
    BLUE
};

Color c = Color::Blue;
```

При этом численно эти константы будут равны 0, 1, 2

---

Можно заставить плюсы приравнивать другие численные значения

```c++
enum class Foo {
    a, // 0
    b, // 1
    c = 10, // 10
    d, // 11
    e = 1, // 1
    f, // 2
    g = f + c // 12
};
```

---

Можно указать какого типа будут эти константы

```c++
enum class Color: int8_t {
    RED,
    GREEN.
    BLUE
};
```

---

## 8.2. Указатели на члены и методы

Представим следующую задачу: у нас есть вектор каких-то сложных структур. Мы хотим его посортить.

Как делать?

* Написать кастомный компаратор
* Еще верней написать лямбду
* Но есть способ пооригинальней

---

```c++
struct S {
    int x = 0;
};

int main() {
    int S::* p = &S::x;

    S s;
    std::cout << s.*p;
}
```

---

Еще можно объявить указатель на метод:

```c++
struct S {
    void f(int a) {}
};

int main() {
    void (S::* p)(int) = &S::f;
    S s;
    (s.*f)(1);
}
```

---

Есть симметричный оператор для указателей:

```c++
struct S {
    void f(int a) {}
};

int main() {
    void (S::* p)(int) = &S::f;
    S s;
    S* p_s = &s;
    (p_s->*f)(1);
}
```

***Note*** ->* можно перегрузить (непонятно зачем)

---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)

---

А теперь опросник :)

Правила следующие:

* На каждый вопрос +- 2 минуты
* Записываете ответ
* В конце будут ответы: считаем баллы
* В конце поймете нужно ли вам подботать плюсы)

---

## Вопрос 1

***Дан следующий код. Компилируется ли этот код? Если нет, то объясните почему***

```c++
#include <iostream>
int& f(int x) {
  return x * x;
}
int main() {
    std::cout << f(5);
}
```

---

## Вопрос 2

***Что выведется на экран, после выполнения следующего кода?***

```c++
#include <iostream>

void f(int value) { value += 1; }

void g(int& value) { value += 1; }

int main() {
  int a = 10;
  int b = 10;

  int& x = a;
  int& y = b;

  f(a); f(b); g(a); g(b);
  f(x); f(y); g(x); g(y);

  std::cout << a << " " << b << " " << x << " " << y << std::endl;
}
```

---

## Вопрос 3

***Будет ли работать данная функция? Если да, то что она вернет для i = 0 и i = 1***

```c++
bool IsZero(int i) {
  return i&&&i;
}
```

---

## Вопрос 4

***Что произойдет после выполнения этого кода?***

```c++
struct S {
  static int GetX() {
    return x;
  }
 private:
  int x = 1;
};

int main() {
  S s;
  std::cout << s.GetX();
}
```

---

## Вопрос 5

***Что произойдет после выполнения этого кода?***

```c++
class C {
  public:
    ~C(int a) {
        std::cout << a;
    }
};

int main() {
    C c;
}
```

---

## Вопрос 6

***Все ли ок с реализацией += и +?***

```c++
class String {
    public:
        friend String operator+(const String& first, const String& second);
        String(const char* str) {...}

        String& operator+=(const String& other) {
            *this = other + *this;
            return *this;
        }
    private:
        std::vector<char> str_;
};

String operator+(const String& first, const String& second) {
    String result = first;
    for (char c : second.str_) {
        first.str_.push_back(c);
    }
    return result;
}
```

---

## Вопрос 7

***В каких строках (1,2,3) будет CE?***

```c++
class Granny {
 protected:
  int g;
  friend int main();
};
class Mom: private Granny {};
class Son: public Mom {
 public:
  void f() {
    Granny granny; // 1
    std::cout << g; // 2
  }
};
Son s;
std::cout << s.m << s.g; // 3
```

---

## Вопрос 8

***Скомпилируется ли данный код? Если нет, то объясните почему.***

```c++
#include <iostream>

struct Base {
  virtual int foo() = 0;
};

int Base::foo() {
  return 5
}

struct Derived: Base {};

int main() {
  Derived d;
  std::cout << d.foo();
}
```

---

## Вопрос 9

***Есть ли проблемы в следующем коде? Если да, то объясните какие***

```c++
template <typename T, typename U>
const T& GetMax(const T& a, const U& b) {
    return a > b ? a : b;
}

int main() {
    GetMax(1, 2.0);
}
```

---

## Вопрос 10

***Работает ли следующий код? Если нет, то объясните почему. Если да, то что выведется на экран?***

```c++
#include <iostream>

template <typename T>
void f(T x) {
    std::cout << 1;
}

template <typename T>
void f(T& x) {
    std::cout << 2;
}

int main() {
    int x = 0;
    int& y = x;
    f(y);
}
```

---

# Ответы

---

## Вопрос 1

***Стоимость - 1 балл***

Ответ: CE. Non-const lvalue reference to type 'int' cannot bind to a temporary of type 'int'

---

## Вопрос 2

***Стоимость - 1 балл***

Ответ: 12 12 12 12

---

## Вопрос 3

***Стоимость - 2 балла***

Ответ: Это же i \&\& (\&i). Более того, указатель на i всегда ненулевой, поэтому это просто сравнение i с нулем, то есть все верно.

---

## Вопрос 4

***Стоимость - 1 балл***

Ответ: CE. Нельзя не статические поля использовать в статических функциях.

---

## Вопрос 5

***Стоимость - 1 балл***

Ответ: CE. Деструктор не умеет принимать аргументы

---

## Вопрос 6

***Стоимость - 2 балла***

Ответ: Нет. += делает лишнюю копию. Нужно + выразить через +=

---

## Вопрос 7

***Стоимость - 2 балла***

Ответ: Во всех трех. Из-за приватного наследования между матерью и бабушкой, сын синтаксически не может использовать слово Granny. (ну и к полю g очевидно тоже обратится не может).

---

## Вопрос 8

***Стоимость - 1 балл***

Ответ: CE. Даже если мы пишем определение pure virtual метода, то класс все равно считается абстрактным

---

## Вопрос 9

***Стоимость - 2 балла***

Ответ: Вернем временно скастованный double к int по ссылке

---

## Вопрос 10

***Стоимость - 1 балл***

Ответ: CE. Так происходит, потому что вызвать функцию от x и от y в данном контексте это одно и тоже. А для x уже будет ambiguous call

---

## Итоги

---

## От 0 до 4

Явно нужно подботать

![img](https://cataas.com/cat/sad)

---

## От 5 до 9

В целом неплохо, но есть куда стремиться

![img](https://cataas.com/cat/gif)

---

## от 9 до 12

Чуть внимательней и все будет отлично

![img](https://cataas.com/cat/funny)

---

## 13 или 14

Все хорошо)

![img](https://cataas.com/cat/laying)