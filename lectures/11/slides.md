---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 11.

---

## 11. Стандартные контейнеры

1. Обзор контейнеров
2. Внутреннее устройство std::vector
3. Внутреннее утройство std::vector<bool>

---

## 11.1 Обзор контейнеров

Контейнеры делятся на две большие группы: последовательные и ассоциативные. Еще существуют "адаптеры" - обертки которые используют внутри контейнеры. 

Рассмотрим какие операции есть у каждого контейнера и за какое время они работают

---

## 11.1.1 Последовательные контейнеры

### std::vector

Вектор - последовательный набор элементов какого-то типа T. Предоставляет следующие методы: 

---

### Доступ к элементам

- at/[]: возвращает элемент по индексу (в первом случае выполняется проверка индекса). Работает за O(1)
- front/back. Возвращает первый/последний элемент. В случае пустого вектора - UB. Работает за O(1)

---

### Модифицирующие операции

- insert/emplace: вставка по итератору. Работает за O(n) (вынуждает весь вектор подвинуться)
- erase: удаление по итератору: O(n)
- push_back/emplace_back: вставка в конец. Работает амортизированно за O(1)
- pop_back: удаление с конца. Работает за O(1). Вызов от пустого вектора: UB

---

### Итератор

- Категория: Contiguous

---

## std::deque

Дек - набор элементов, который поддерживает вставку как в конец так и в начало. Хранит элементы в "бакетах"

---

### Доступ к элементам

- at/[]: возвращает элемент по индексу (в первом случае выполняется проверка индекса). Работает за O(1)
- front/back. Возвращает первый/последний элемент. В случае пустого дека - UB. Работает за O(1)

---

### Модифицирующие операции

- insert/emplace: вставка по итератору. Работает за O(n)
- erase: удаление по итератору: O(n)
- push_back/emplace_back/push_front/emplace_front: вставка в конец/начало. Работает амортизированно за O(1)
- pop_back/pop_front: удаление с конца/начала. Работает за O(1). Вызов от пустого дека: UB

---

### Итератор

- Категория: RandomAccess

---

### std::list

Обычный двусвязный список

---

### Доступ к элементам

- front/back. Возвращает первый/последний элемент. В случае пустого листа - UB. Работает за O(1)

---

### Модифицирующие операции

- insert/emplace: вставка по итератору. Работает за O(1)
- erase: удаление по итератору: O(1)
- push_back/emplace_back/push_front/emplace_front: вставка в конец/начало. Работает за O(1)
- pop_back/pop_front: удаление с конца/начала. Работает за O(1)

---

### Итератор

- Категория: BidirectionalIterator

---

### std::forward_list

Обычный односвязный список

### Доступ к элементам

- front. Возвращает первый элемент. В случае пустого листа - UB. Работает за O(1)

---

### Модифицирующие операции

- insert/emplace: вставка по итератору. Работает за O(1)
- erase: удаление по итератору: O(1)
- push_front/emplace_front: вставка в начало. Работает за O(1)
- pop_front: удаление с начала. Работает за O(1)

---

### Итератор

- Категория: ForwardIterator

---

## 11.1.2 Ассоциативные контейнеры

## std::map

Отсортированный по ключу ассоциативный контейнер который хранит пары ключ-значение. Внутри обычно используется красно-черное дерево поиска. 

---

### Доступ к элементам

- index[] - доступ по ключу. Модифицрует контейнер - если значения с таким ключом не существует то создает его (кладет туда Value()). Работает за O(logn)
- at - доступ по ключу. Если значения с таким ключом не существует то выкидывает ошибку. Работает за O(logn)
- find - возвращает итератор на пару с нужным ключем. Если ключа нет возвращает end(). Работает за O(logn)

---

### Модифицирующие операции

- insert/emplace: вставка по итератору. Работает за O(logn)
- erase: удаление по итератору: O(logn)

---

### Итератор

- Категория: BidirectionalIterator

---

## std::set

Множество элементов. По сути это map без value :) 

---

## std::unordered_map

Ассоциативный контейнер который хранит пары ключ-значение. Является хэш-таблицей

---

### Доступ к элементам

- index[] - доступ по ключу.  Модифицрует контейнер - если значения с таким ключом не существует то создает его (кладет туда Value()). Работает за O(1) (не совсем честно). 
- at - доступ по ключу. Если значения с таким ключом не существует то выкидывает ошибку. Работает за O(1) (не совсем честно). 
- find - возвращает итератор на пару с нужным ключем. Если ключа нет возвращает end(). Работает за O(1) (не совсем честно). 

---

### Модифицирующие операции

- insert/emplace: вставка по итератору. Работает за O(1) (не совсем честно). 
- erase: удаление по итератору: Работает за O(1) (не совсем честно). 

---

### Итератор

- Категория: ForwardIterator

---

## std::unordered_set

Неупорядоченное множество элементов. По сути это unordered_map без value :) 

---

Еще существуют multimap и multiset - подедрижвают одинаковые ключи. 

---

## 11.1.3 Адаптеры 

В stl существуют так же адаптеры над контейнерами: stack, queue, priority_queue.
Работают они довольно просто: используют методы контейнера который хранят в полях. Например std::stack использует std::deque по умолчанию


---

## 11.2 Внутреннее устройство std::vector


Возьмем простую загшотовку вектора: 

```c++
template <typename T>
class Vector {
 public:
  explicit Vector(size_t n, const T& value = T());

  T& operator[](size_t index);
  const T& operator[](size_t index) const;

  T& at(size_t index);

  const T& at(size_t index);

  size_t size() const {return size_; }
  size_t capacity() const {return capacity_; }
 private:
  T* arr_ = nullptr;
  size_t capacity_ = 0;
  size_t size_ = 0;
}
```

---

## reserve VS resize 

reserve - это про capacity
resize - это про size

```c++
std::vector<int> v;
for (int i = 0; i < 50; ++i) {
  v.push_back(i);
  std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

- capacity и size разлечаются 

---

```c++
std::vector<int> v(50, 1);
for (int i = 0; i < 50; ++i) {
  v.pop_back()
  v.shrink_to_fit();
  std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

- capacity не уменьшается (даже если вызвать clear)
- чтобы уменьшить вектор нужно вызвать shrink_to_fit

---

## reserve - первая попытка

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = new T[n];
  for (size_t i = 0; i < size_; ++i) {
    new_arr[i] = arr[i];
  }

  delete[] arr;
  arr = new_arr;
  capacity_ = n;
}
```
 
Проблемы: 

- T* new_arr = new T[n]: может не быть конструктора по умолчанию
- Мы зачем-то создаем объекты, хотя нас об этом не просили

---

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new_arr[i] = arr[i];
  }

  delete[] arr;
  arr = new_arr;
  capacity_ = n;
}
```

Уже лучше, но проблемы все еще есть: 

- В строке new_arr[i] = arr[i] сегфолт (в new_arr[i] голые байты, там нет сформированного объекта)

---

Нужно как-то вызвать конструктор T на памяти arr[i]. Для этого существует оператор placement new: ```new (ptr) T(args)```

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new(new_arr + i) T(arr[i]); // тут воспользовались placement new
  }

  delete[] arr;
  arr = new_arr;
  capacity_ = n;
}
```

И опять сегфолт....

---

Нужно исправить удаление 

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new(new_arr + i) T(arr[i]);
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

---

Остались следующие проблемы: 

- Копирование это плохо :( 
- Небезопасно относительно исключений (если конструктор копирования бросит исключение, то мы не освободим нашу память)

Исправить пока можем только вторую проблему

---

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  size_t i = 0;
  try {
    for (i < size_; ++i) {
      new(new_arr + i) T(arr[i]);
    }
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      (new_arr + j)->~T();
    }
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }
  

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

---

Чтобы не писать такой блок каждый раз можно воспользоваться std::uninitialized_copy: 

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  try {
    std::uninitialized_copy(arr_, arr_ + size_, new_arr);
  } catch (...) {
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

---

Теперь можно и push_back написать 

```c++
template <typename T>
void Vector<T>::push_back(const T& value) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(value);
  size_ += 1;
} 
```

---

Ну и pop_back назодно

```c++
template <typename T>
void Vector<T>::pop_back(const T& value) {
  (arr + size_ - 1)->~T();
  size_ -= 1;
} 
```

---

## 11.3 Внутреннее устройство std::vector<bool>

Хочется чтобы вектор из булов тратил по биту на каждый бул. Если использовать обычный вектор, то на каждый бул будет расходываться по байту. 

В stl есть специализация шаблона std::vector для bool 

---

Посмотрим на следующий код 

```c++
template <typename T>
void f() = delete;

std::vector<bool> v(10, false);
v[5] = true;
f(v[5]);
```

Увидим что на самом деле v[i] это std::_Bit_reference

---

Примерная реализация выглядит так: 

```c++
template <>
class Vector<bool> {
 public:
  struct BitReference {
    int8_t* cell;
    uint8_t index;

    BitReference& operator=(bool b) {
      if (b) {
        *cell |= (1u << num);
      } else {
        *cell &= ~(1u << num);
      }
    }

    operator bool() const {
      return *cell & (1u << num);
    }
  }

  BitReference operator[](size_t i) {
      return BitReference(arr_ + i / 8, i % 8);
  }
 private:
  int8_t* arr;
  size_t size_;
  size_t capacity_;
};
```

---

BitReference это отличный контрпример к сишному определению lvalue/rvalue :) 

---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)
