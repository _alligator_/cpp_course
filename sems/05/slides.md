---
theme: gaia
header: 'Программирование на языке C++'
footer: 'Полторак Семён. ВШПИ МФТИ 2023'
marp: true
---

# Семинар 5

---

## Перегрузка операторов

- Возможность применять операторы языка к сложным типам данных, в том числе пользовательским
- Позволяет избегать копипасты, помогает переиспользовать код
- При перегрузке стараемся сохранять смысл
- +, -, *, /, %, ^, &, |, ~, !, ,, =, <, >, <=, >=, ++, –-, <<, >>, ==, !=, &&, ||, +=, -=, /=, %=, ^=, &=, |=, *=, [], (), ->, ->*, new, new[], delete, delete[]

---

## Перегрузка оператора сравнения

- `void operator=(const int& rhs);`

---

## Перегрузка оператора []

- Реализуется только как функция-член
- `T& operator[](int ind);`
- `const T& operator[](int ind) const;`
- https://godbolt.org/z/K9v55sdaq CPP23

---

## Перегрузка +

- Может быть перегружен как функция-член и как свободная функция
- `X operator+() const;`
- `X operator+(const X& x) const;`
- `X operator+(X const& c1, X const& c2);`
- `Pos& operator+=(const Pos& rhs);`

---

* Стоит-ли делать перегрузку фукцией-членом?
* Почему выражаем + через +=?
* Делать-ли friend?

---

## hidden friends

- friend - открывает для функции доступ к приватным полям класса
- Есть подводные камни с неквалифицированным именем
- Почитать больше https://habr.com/ru/articles/656411/

---

## Return Value Optimization

* Что работает быстрее: `a += b; return a;` или `return a += b;`?
* Происходит избегание лишнего копирования (copy ellision). Оптимизация происходит на этапе локальной компиляции.
* Конструктор возвращаемого объекта не должен быть explicit
* Возвращаться должен объекьт, а не ссылка

---

## Инкременты

- Постфиксный и префиксный реализуются как функции-члены
- Постфиксный определяется через префиксный
- `Pos& operator++();`
- `const Pos operator++(int);`

---

- Какой инкремент быстрее?

---

## Перегрузка операторов сравнения

- Перегружаются как свободные функции
- Базовые операторы это `<` и `==`, через них выражаются остальные
- `bool operator==(const X& lh, const X& rh);`
- `bool operator<(const X& lh, const X& rh);`

---

## spaceship-оператор (C++20)

```
(A <=> B) < 0 is true if A < B
(A <=> B) > 0 is true if A > B
(A <=> B) == 0 is true if A and B are equal/equivalent.
```

---

## Оператор приведения типа

```cpp
operator type() {
    // implementation
}
```

---

```cpp
class MyClass {
  int i;
  explicit MyClass(int i) : i(i) {}
}

// ...

int main() {
  MyClass clz = MyClass(2);
}
```

---

```cpp
class MyClass {
  int i;
  MyClass(int i) : i(i) {}
}

// ...

int main() {
  MyClass clz = 2
}
```


---

* Не забываем про explicit для конструкторов от одного аргумента и операторов приведения
* contextual conversion

---

## Literals

- Выражение -- константа времени компиляции
- Перегрузка возможна не для всхе литералов https://learn.microsoft.com/en-us/cpp/cpp/user-defined-literals-cpp?view=msvc-170#user-defined-literal-operator-signatures

```cpp
long double operator "" _inv(long double val) {
  return -val;
}

std::cout << 5_inv;
```
