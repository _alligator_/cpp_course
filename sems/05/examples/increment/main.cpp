#include <iostream>

struct Pos {
  int x, y;

  Pos& operator++() {
    std::cout << "Called prefix" << std::endl;
    ++x;
    ++y;

    return *this;
  }

  Pos operator++(int) {
    std::cout << "Called postfix" << std::endl;
    Pos its = *this;
    ++*this;
    return its;
  }
};

int main() {
  Pos var;
  std::cin >> var.x >> var.y;

  var++;

  ++var;

  return 0;
}
