#include <iostream>

struct MyInteger {
  int number;

  void operator=(const int& rhs) { number = rhs; }
};

int main() {
  MyInteger var;
  std::cin >> var.number;

  std::cout << var.number << std::endl;

  return 0;
}
