#include <iostream>

const int kIdX = 0;
const int kIdY = 1;

struct Pos {
  int x, y;

  int& operator[](int idx) {
    if (idx == kIdX) {
      return x;
    }
    if (idx == kIdY) {
      return y;
    }
    std::abort();
  }

  const int& operator[](int idx) const {
    if (idx == kIdX) {
      return x;
    }
    if (idx == kIdY) {
      return y;
    }
    std::abort();
  }
};

void PrintPosition(const Pos& var) {
  std::cout << "X = " << var[kIdX] << "; Y = " << var[kIdY] << std::endl;
}

int main() {
  Pos var;
  std::cin >> var[kIdX] >> var[kIdY];

  PrintPosition(var);

  return 0;
}
