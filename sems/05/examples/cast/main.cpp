#include <iostream>

struct InvPos {
  int y, x;
};

struct Pos {
  int x, y;

  operator InvPos() const {
    InvPos pos;
    pos.x = y;
    pos.y = x;

    return pos;
  }
};

int main() {
  Pos val;
  std::cin >> val.x >> val.y;

  InvPos ival0 = val;
  InvPos ival1 = static_cast<InvPos>(val);

  std::cout << val.x << " " << val.y << std::endl;
  std::cout << ival0.x << " " << ival0.y << std::endl;
  std::cout << ival1.x << " " << ival1.y << std::endl;

  return 0;
}
