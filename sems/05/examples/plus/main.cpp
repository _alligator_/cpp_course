#include <iostream>

struct Pos {
  int x, y;

  Pos& operator+=(const Pos& rhs) {
    x += rhs.x;
    y += rhs.y;

    return *this;
  }

  Pos operator+() const { return *this; }
};

Pos operator+(const Pos& lhs, const Pos& rhs) {
  Pos buffer = lhs;
  buffer += rhs;

  return buffer;
}

int main() {
  Pos var;

  std::cin >> var.x >> var.y;

  Pos temp = +var;  // Pos operator+(const Pos& val) (unary plus)
  std::cout << temp.x << " " << temp.y << std::endl;

  temp = temp + var;  // Pos operator+(const Pos& lhs, const Pos& rhs)
  std::cout << temp.x << " " << temp.y << std::endl;

  return 0;
}
