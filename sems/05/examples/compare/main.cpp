#include <iostream>
#include <utility>

struct Pos {
  int x, y;
};

bool operator==(const Pos& lhs, const Pos& rhs) {
  return lhs.x == rhs.x && lhs.y == rhs.y;
}

bool operator<(const Pos& lhs, const Pos& rhs) {
  return lhs.x < rhs.x && lhs.y < rhs.y;
}

bool operator>(const Pos& lhs, const Pos& rhs) {
  return std::rel_ops::operator>(lhs, rhs);
}

bool operator>=(const Pos& lhs, const Pos& rhs) {
  return std::rel_ops::operator>=(lhs, rhs);
}

bool operator<=(const Pos& lhs, const Pos& rhs) {
  return std::rel_ops::operator<=(lhs, rhs);
}

bool operator!=(const Pos& lhs, const Pos& rhs) {
  return std::rel_ops::operator!=(lhs, rhs);
}

int main() {
  Pos lhs;
  std::cin >> lhs.x >> lhs.y;

  Pos rhs;
  rhs.x = lhs.x + 1;
  rhs.y = lhs.y + 1;

  std::cout << "lhs < rhs: " << std::boolalpha << (lhs < rhs) << std::endl;
  std::cout << "lhs == rhs: " << std::boolalpha << (lhs == rhs) << std::endl;

  std::cout << "lhs > rhs: " << std::boolalpha << (lhs > rhs) << std::endl;
  std::cout << "lhs >= rhs: " << std::boolalpha << (lhs >= rhs) << std::endl;
  std::cout << "lhs <= rhs: " << std::boolalpha << (lhs <= rhs) << std::endl;
  std::cout << "lhs != rhs: " << std::boolalpha << (lhs != rhs) << std::endl;

  return 0;
}
