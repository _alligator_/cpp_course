#include <iostream>

void PrintVars(int first_var, int second_var) {
  int third_var = first_var;
  int fourth_var = second_var;
}

int main() {
  int first_var = 0;
  int second_var = 0;
  std::cin >> first_var >> second_var;

  PrintVars(first_var, second_var);

  return 0;
}
