#include <iostream>

int main() {
  int super_var = 0;
  if (super_var == 0) {
    return -42;  // NOLINT
  }

  std::cout << super_var;  // Unreachable code

  return 0;
}
