#include <iostream>

#include "my_function.hpp"

int main(int argc, char** argv) {
  std::cout << MyFunction() << std::endl;
  return 0;
}
