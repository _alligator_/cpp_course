# Семинары

## Первый семестр:

- Семинар 1 [презентация](https://yaishenka.gitlab.io/cpp_course/sems/01) [сурс](/sems/01/slides.md)
- Семинар 2 [презентация](https://yaishenka.gitlab.io/cpp_course/sems/02) [сурс](/sems/02/slides.md)
- Семинар 3 [презентация](https://yaishenka.gitlab.io/cpp_course/sems/03) [сурс](/sems/03/slides.md)
- Семинар 4 [презентация](https://yaishenka.gitlab.io/cpp_course/sems/04) [сурс](/sems/04/slides.md)
- Семинар 5 [презентация](https://yaishenka.gitlab.io/cpp_course/sems/05) [сурс](/sems/05/slides.md)
- Семинар 6 [презентация](https://yaishenka.gitlab.io/cpp_course/sems/06) [сурс](/sems/06/slides.md)
- Семинар 7 TODO
- Семинар 8 [презентация](https://yaishenka.gitlab.io/cpp_course/sems/08) [сурс](/sems/08/slides.md)
