#include <cassert>
#include <iostream>

int TwoSum(int first_number, int second_number) {
  return abs(first_number + second_number);
}

void TestTwoSum() {
  assert(TwoSum(1, 10) == 11);
  assert(TwoSum(-1, 1) == 0);
  assert(TwoSum(0, 1) == 1);
  assert(TwoSum(-1, 0) == -1);  // Fail is here
}

int main() {
  TestTwoSum();

  int first_number = 0;
  int second_number = 0;

  std::cin >> first_number >> second_number;
  std::cout << TwoSum(first_number, second_number) << std::endl;

  return 0;
}
