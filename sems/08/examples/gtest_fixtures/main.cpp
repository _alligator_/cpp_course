#include <gtest/gtest.h>

#include <iostream>

class Foo {
   public:
  Foo() : idx(0) { std::cout << "CONSTRUCTED" << std::endl; }
  ~Foo() { std::cout << "DESTRUCTED" << std::endl; }
  int idx;
};

class MyFixture : public ::testing::Test {
   protected:
  void SetUp() {
    foo_ = new Foo;
    foo_->idx = 5;  // NOLINT
  }
  void TearDown() { delete foo_; }
  Foo* foo_;
};

TEST_F(MyFixture, test1) {
  ASSERT_EQ(foo_->idx, 5);
  foo_->idx = 10;  // NOLINT
}

int main(int argc, char* argv[]) {
  ::testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
