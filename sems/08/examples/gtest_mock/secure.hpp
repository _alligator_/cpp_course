#pragma once
#include <string>

class DBClient {
   public:
  DBClient() : is_authorized_(false) {}
  virtual bool authorize(const std::string& password);
  virtual std::string get_password();

   private:
  bool is_authorized_;
};
