#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "secure.hpp"

class MockDBClient : public DBClient {
   public:
  MOCK_METHOD(std::string, get_password, ());
};

TEST(SecureSuit, AuthorizeTest) {
  MockDBClient mock_client;
  const std::string kRealPassword = "real_password";
  EXPECT_CALL(mock_client, get_password())
      .Times(1)
      .WillRepeatedly(testing::Return(kRealPassword));

  EXPECT_TRUE(mock_client.authorize(kRealPassword));

  const std::string kFakePassword = "fake";
  EXPECT_CALL(mock_client, get_password())
      .Times(1)
      .WillRepeatedly(testing::Return(kFakePassword));

  EXPECT_FALSE(mock_client.authorize(kRealPassword));
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  ::testing::InitGoogleMock(&argc, argv);

  return RUN_ALL_TESTS();
}
