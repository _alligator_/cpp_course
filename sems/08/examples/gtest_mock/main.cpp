#include <iostream>

#include "secure.hpp"

int main() {
  std::string password;
  std::cin >> password;
  std::cout << std::boolalpha;
  DBClient client;
  std::cout << client.authorize(password) << std::endl;

  return 0;
}