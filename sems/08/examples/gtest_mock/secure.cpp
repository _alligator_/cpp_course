#include "secure.hpp"

#include <iostream>

bool DBClient::authorize(const std::string& password) {
  is_authorized_ = password == get_password();

  return is_authorized_;
}

std::string DBClient::get_password() {
  const char* password = std::getenv("SECRET_PASSWORD");
  if (password != nullptr) {
    return std::string(password);
  }

  return "";
}
