#include <iostream>

void Foo() { throw std::runtime_error("oops"); }
void Fizz() {}
void Bar() noexcept {}
void Lol() noexcept { throw std::logic_error("kek"); }
struct Struct {
  Struct() noexcept(false) {}
  ~Struct() noexcept(true) {}
};

int main() {
  std::cout << noexcept(Foo()) << '\n';     // 0
  std::cout << noexcept(Fizz()) << '\n';    // 0
  std::cout << noexcept(Lol()) << '\n';     // 1
  std::cout << noexcept(Bar()) << '\n';     // 1
  std::cout << noexcept(Struct()) << '\n';  // 1

  return 0;
}
