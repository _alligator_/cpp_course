#include <exception>
void Foo() { throw 4; }  // NOLINT

void Fzz() {
  try {
    Foo();
  } catch (...) {
    throw;
  }
}

int main() { Fzz(); }
