#include <iostream>

void Foo() {
  int k = 0;  // NOLINT
  throw k;
}

int main() {
  try {
    Foo();
  } catch (double x) {  // NOLINT
    std::cout << "print x" << std::endl;
  }

  return 0;
}
