#include <iostream>

struct Struct {
  Struct() { std::cout << "create\n"; }

  Struct(const Struct&) {  // NOLINT
    std::cout << "copy\n";
  }

  ~Struct() { std::cout << "destroy\n"; }
};

void Foo() {
  Struct empty_struct;
  throw empty_struct;
}

int main() {
  try {  // first attemp
    Foo();
  } catch (const Struct& fail) {
    std::cout << "caught\n";
  }

  try {  // second attemp
    Foo();
  } catch (Struct fail) {
    std::cout << "caught\n";
  }
}
