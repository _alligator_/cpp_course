#include <iostream>

void Foo() noexcept { throw 42; }  // NOLINT

int main() {
  try {
    Foo();
  } catch (...) {
    std::cout << "The exception caught" << std::endl;
  }
}
