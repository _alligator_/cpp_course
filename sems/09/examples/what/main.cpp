#include <exception>
#include <iostream>

void Foo() { throw std::runtime_error("text of exeception"); }

int main() {
  try {
    Foo();
  } catch (std::runtime_error& err) {
    std::cout << err.what() << std::endl;
  }

  return 0;
}