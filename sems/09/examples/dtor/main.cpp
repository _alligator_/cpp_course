#include <iostream>

struct S {
  int x0;
  int y0;

  S() {
    x0 = 0;
    y0 = 0;
  }

  S(int x, int y) {  // NOLINT
    std::cout << "Call ctor" << std::endl;
    x0 = x;
    throw 1;  // NOLINT
    y0 = y;
  }

  ~S() { std::cout << "Call dtor" << std::endl; }
};

int main() {
  S str;
  try {
    str = S(50, 70);  // NOLINT
  } catch (...) {
    std::cout << str.x0 << " " << str.y0 << std::endl;
  }

  std::cout << "End try...catch" << std::endl;

  return 0;
}